# Case Proje Kurulumu
Aşağıdaki komutları çalıştırıyoruz.

    docker-compose build
    docker-compose up -d

Docker containerlar ayağı kalktıktan sonra ise bu sefer aşağıdaki komutu çalıştırıp app makinesine bağlanıyoruz.

    docker exec -it case_app bash

App makinesine bağlandıktan sonra ise hem composer update yapıp hem de migrationları oluşturuyoruz.

    composer install
    php artisan migrate