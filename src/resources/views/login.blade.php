<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
    <style>
        body {
            background-color: #f4f7fc;
        }
        .container {
            max-width: 400px;
            margin-top: 100px;
        }
        .message {
            margin-top: 20px;
            font-size: 16px;
        }
        .message.success {
            color: #28a745;
        }
        .message.error {
            color: #dc3545;
        }
    </style>
</head>
<body>
<div class="container">
    <h2 class="text-center">Login</h2>
    <form id="login-form">
        <div class="form-group">
            <label for="email">Email</label>
            <input type="email" class="form-control" id="email" placeholder="Enter email" required>
        </div>
        <div class="form-group">
            <label for="password">Password</label>
            <input type="password" class="form-control" id="password" placeholder="Password" required>
        </div>
        <button type="submit" class="btn btn-primary btn-block">Login</button>
    </form>
    <div id="message" class="message text-center"></div>
</div>

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.2/dist/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

<script>
    const API_URL = 'http://localhost/api/v1';

    document.getElementById('login-form').addEventListener('submit', async function(event) {
        event.preventDefault();

        const email = document.getElementById('email').value;
        const password = document.getElementById('password').value;
        const messageElement = document.getElementById('message');

        try {
            const response = await fetch(`${API_URL}/login`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({ email, password }),
            });

            let data;
            try {
                data = await response.json();
            } catch (error) {
                console.error('JSON parse error:', error);
                data = { error: 'Unexpected error occurred' };
            }

            if (response.ok) {
                if (data.original && data.original.access_token) {
                    localStorage.setItem('token', data.original.access_token);
                    messageElement.textContent = 'Login Successful!';
                    messageElement.classList.remove('error');
                    messageElement.classList.add('success');

                    setTimeout(() => {
                        window.location.href = 'http://localhost/addrecord';
                    }, 1000);
                } else {
                    messageElement.textContent = `Error: ${data.original?.error || 'Invalid credentials'}`;
                    messageElement.classList.remove('success');
                    messageElement.classList.add('error');
                }
            } else {
                messageElement.textContent = `Error: ${data.original?.error || 'Invalid credentials'}`;
                messageElement.classList.remove('success');
                messageElement.classList.add('error');
            }
        } catch (error) {
            console.error('Request failed:', error);
            messageElement.textContent = 'Network error. Please try again later.';
            messageElement.classList.remove('success');
            messageElement.classList.add('error');
        }
    });
</script>
</body>
</html>
