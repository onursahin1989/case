<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Add Record</title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
</head>
<body>
<div class="container">
    <h2 class="text-center mt-5">Add Record</h2>
    <form id="add-record-form">
        <div class="form-group">
            <label for="name">Name</label>
            <input type="text" class="form-control" id="name" required>
        </div>
        <div class="form-group">
            <label for="birthday">Birthday</label>
            <input type="date" class="form-control" id="birthday" required>
        </div>
        <div class="form-group">
            <label for="gender">Gender</label>
            <select class="form-control" id="gender" required>
                <option value="male">Male</option>
                <option value="female">Female</option>
                <option value="other">Other</option>
            </select>
        </div>
        <div class="form-group">
            <label for="address">Address</label>
            <input type="text" class="form-control" id="address" required>
        </div>
        <div class="form-group">
            <label for="post_code">Post Code</label>
            <input type="text" class="form-control" id="post_code" required>
        </div>
        <div class="form-group">
            <label for="city_name">City Name</label>
            <input type="text" class="form-control" id="city_name" required>
        </div>
        <div class="form-group">
            <label for="country_name">Country Name</label>
            <input type="text" class="form-control" id="country_name" required>
        </div>
        <button type="submit" class="btn btn-primary btn-block">Submit</button>
    </form>
    <div id="message" class="text-center mt-3"></div>
</div>

<script>
    const API_URL = 'http://localhost/api/v1';
    const token = localStorage.getItem('token');

    if (!token) {
        alert('Unauthorized access! Redirecting to login...');
        window.location.href = 'login';
    }

    document.getElementById('add-record-form').addEventListener('submit', async function(event) {
        event.preventDefault();

        const recordData = {
            name: document.getElementById('name').value,
            birthday: document.getElementById('birthday').value,
            gender: document.getElementById('gender').value,
            address: document.getElementById('address').value,
            post_code: document.getElementById('post_code').value,
            city_name: document.getElementById('city_name').value,
            country_name: document.getElementById('country_name').value
        };

        try {
            const response = await fetch(`${API_URL}/add-record`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${token}`
                },
                body: JSON.stringify(recordData)
            });

            const data = await response.json();

            if (response.ok) {
                document.getElementById('message').textContent = 'Record added successfully! Redirecting...';
                setTimeout(() => {
                    window.location.href = 'person';
                }, 2000);
            } else {
                document.getElementById('message').textContent = `Error: ${data.error || 'Failed to add record'}`;
            }
        } catch (error) {
            console.error('Request failed:', error);
            document.getElementById('message').textContent = 'Network error. Please try again later.';
        }
    });
</script>
</body>
</html>
