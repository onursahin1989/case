<!DOCTYPE html>
<html lang="tr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Kayıt Listesi</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<div class="container mt-4">
    <h2 class="mb-4 text-center">Records</h2>
    <table class="table table-bordered">
        <thead class="table-dark">
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Birthday</th>
            <th>Gender</th>
            <th>Address</th>
            <th>Post Code</th>
            <th>City Name</th>
            <th>Country Name</th>
        </tr>
        </thead>
        <tbody id="records-table">
        </tbody>
    </table>
</div>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js"></script>

<script>
    document.addEventListener("DOMContentLoaded", function () {
        fetchRecords();
    });

    function fetchRecords() {
        fetch("{{ url('http://localhost/api/v1/persons') }}") // API URL'ini güncelle
            .then(response => response.json())
            .then(data => {
                let tableBody = document.getElementById("records-table");
                tableBody.innerHTML = ''; // Önce tabloyu temizle
                data.forEach(record => {
                    let row = document.createElement("tr");
                    row.innerHTML = `
                        <td>${record.id}</td>
                        <td>${record.name}</td>
                        <td>${record.birthday}</td>
                        <td>${record.gender}</td>
                        <td>${record.address.address}</td>
                        <td>${record.address.post_code}</td>
                        <td>${record.address.city_name}</td>
                        <td>${record.address.country_name}</td>
                    `;
                    tableBody.appendChild(row);
                });
            })
            .catch(error => console.error("API hatası:", error));
    }
</script>
</body>
</html>
