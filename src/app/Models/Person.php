<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
    protected $table = 'persons';
    protected $fillable = [
        'name',
        'birthday',
        'gender',
        'address_id'
    ];

    public function address()
    {
        return $this->belongsTo(Address::class, 'address_id', 'id');
    }
}
