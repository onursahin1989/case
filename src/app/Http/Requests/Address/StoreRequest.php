<?php

namespace App\Http\Requests\Address;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'address' => ['required', 'string'],
            'post_code' => ['required', 'string'],
            'city_name' => ['required', 'string'],
            'country_name' => ['required', 'string'],
        ];
    }
}
