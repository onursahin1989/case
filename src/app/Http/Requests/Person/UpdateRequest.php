<?php

namespace App\Http\Requests\Person;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'name' => ['required', 'string'],
            'birthday' => ['required', 'dateFormat:Y-m-d'],
            'gender' => ['required', 'string'],
            'address_id' => ['required', 'exists:addresses,id'],
        ];
    }
}
