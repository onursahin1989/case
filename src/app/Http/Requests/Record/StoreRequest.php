<?php

namespace App\Http\Requests\Record;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'name' => ['required', 'string'],
            'birthday' => ['required', 'dateFormat:Y-m-d'],
            'gender' => ['required', 'string'],
            'address' => ['required', 'string'],
            'post_code' => ['required', 'string'],
            'city_name' => ['required', 'string'],
            'country_name' => ['required', 'string'],
        ];
    }
}
