<?php

namespace App\Http\Controllers\Api\Auth;

use App\Business\AuthManager;
use App\Http\Requests\Api\Auth\LogoutRequest;

class LogoutController
{
    public function __invoke(LogoutRequest $request, AuthManager $authManager)
    {
        $result = $authManager->logout();

        return response()->json($result);
    }
}
