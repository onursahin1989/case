<?php

namespace App\Http\Controllers\Api\Auth;

use App\Business\AuthManager;
use App\Http\Requests\Api\Auth\RegisterRequest;

class RegisterController
{
    /**
     * @param  RegisterRequest  $request
     * @param  AuthManager  $authManager
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(RegisterRequest $request, AuthManager $authManager)
    {
        $result = $authManager->register(
            $request->validated()
        );

        return response()->json($result, 201);
    }
}
