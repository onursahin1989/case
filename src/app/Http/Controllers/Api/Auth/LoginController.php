<?php

namespace App\Http\Controllers\Api\Auth;

use App\Business\AuthManager;
use App\Http\Requests\Api\Auth\LoginRequest;

class LoginController
{
    public function __invoke(LoginRequest $request, AuthManager $authManager){
        $result = $authManager->login(
            $request->validated()
        );

        return response()->json($result, 200);
    }
}
