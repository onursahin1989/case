<?php

namespace App\Http\Controllers\Address;

use App\Business\AddressManager;
use App\Http\Controllers\Controller;
use App\Http\Requests\Address\StoreRequest;

class StoreController extends Controller
{
    /**
     * @param  StoreRequest  $request
     * @param  AddressManager  $addressManager
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(StoreRequest $request, AddressManager $addressManager)
    {
        $result = $addressManager->storeAddress(
            $request->validated()
        );

        return response()->json($result, 201);
    }
}
