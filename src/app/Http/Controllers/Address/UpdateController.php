<?php

namespace App\Http\Controllers\Address;

use App\Business\AddressManager;
use App\Http\Controllers\Controller;
use App\Http\Requests\Address\UpdateRequest;

class UpdateController extends Controller
{
    /**
     * @param  UpdateRequest  $request
     * @param  int  $id
     * @param  AddressManager  $addressManager
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(UpdateRequest $request, int $id, AddressManager $addressManager)
    {
        $result = $addressManager->updateAddress(
            $request->validated(),
            $id
        );

        return response()->json($result, 200);
    }
}
