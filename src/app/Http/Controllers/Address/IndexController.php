<?php

namespace App\Http\Controllers\Address;

use App\Business\AddressManager;
use App\Http\Controllers\Controller;
use App\Http\Requests\Address\IndexRequest;

class IndexController extends Controller
{
    /**
     * @param  IndexRequest  $request
     * @param  AddressManager  $addressManager
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(IndexRequest $request, AddressManager $addressManager)
    {
        $result = $addressManager->getAddresses();

        return response()->json($result, 200);
    }
}
