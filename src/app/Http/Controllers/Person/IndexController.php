<?php

namespace App\Http\Controllers\Person;

use App\Business\PersonManager;
use App\Http\Controllers\Controller;
use App\Http\Requests\Person\IndexRequest;

class IndexController extends Controller
{
    /**
     * @param  IndexRequest  $request
     * @param  PersonManager  $personManager
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(IndexRequest $request, PersonManager $personManager)
    {
        $result = $personManager->getPersons();

        return response()->json($result, 200);
    }
}
