<?php

namespace App\Http\Controllers\Person;

use App\Business\PersonManager;
use App\Helpers\ApiResponse;
use App\Http\Controllers\Controller;
use App\Http\Requests\Person\UpdateRequest;

class UpdateController extends Controller
{
    /**
     * @param  UpdateRequest  $request
     * @param  int  $id
     * @param  PersonManager  $personManager
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(UpdateRequest $request, int $id, PersonManager $personManager)
    {
        $result = $personManager->updatePerson(
            $request->validated(),
            $id
        );

        return response()->json($result, 200);
    }
}
