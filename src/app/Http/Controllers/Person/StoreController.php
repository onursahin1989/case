<?php

namespace App\Http\Controllers\Person;

use App\Business\PersonManager;
use App\Helpers\ApiResponse;
use App\Http\Controllers\Controller;
use App\Http\Requests\Person\StoreRequest;

class StoreController extends Controller
{
    /**
     * @param  StoreRequest  $request
     * @param  PersonManager  $personManager
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(StoreRequest $request, PersonManager $personManager)
    {
        $result = $personManager->storePerson(
            $request->validated()
        );

        return response()->json($result, 201);
    }
}
