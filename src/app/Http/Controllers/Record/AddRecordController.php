<?php

namespace App\Http\Controllers\Record;

use App\Business\RecordManager;
use App\Http\Controllers\Controller;
use App\Http\Requests\Record\StoreRequest;

class AddRecordController extends Controller
{
    /**
     * @param  StoreRequest  $request
     * @param  RecordManager  $recordManager
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(StoreRequest $request, RecordManager $recordManager)
    {
        $result = $recordManager->addRecord(
            $request->validated()
        );

        return response()->json($result, 201);
    }
}
