<?php

namespace App\Business;

use App\Models\Person;
use Illuminate\Support\Facades\Cache;

class PersonManager
{
    public function storePerson(array $data)
    {
        $person = Person::create($data);

        Cache::forget('persons');

        return $person;
    }

    public function getPersons()
    {
        $persons = Cache::remember('persons', 3600, function () {
            return Person::with('address')->get();
        });

        return $persons;
    }

    public function updatePerson(array $data, int $id)
    {
        $person = Person::findOrFail($id);
        $person->update($data);

        return $person;
    }
}
