<?php

namespace App\Business;

use App\Models\Address;

class AddressManager
{
    public function storeAddress(array $data)
    {
        return Address::create($data);
    }

    public function getAddresses()
    {
        return Address::paginate();
    }

    public function updateAddress(array $data, int $id)
    {
        $address = Address::findOrFail($id);
        $address->update($data);

        return $address;
    }
}
