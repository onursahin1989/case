<?php

namespace App\Business;

class RecordManager
{
    public function addRecord(array $data)
    {
        $addressData = [
            'address' => $data['address'],
            'post_code' => $data['post_code'],
            'city_name' => $data['city_name'],
            'country_name' => $data['country_name'],
        ];

        $addressManager = resolve(AddressManager::class);
        $address = $addressManager->storeAddress($addressData);

        if(!$address){
            throw new \Exception('ADDRESS_DOES_NOT_STORE');
        }

        $personData = [
            'name' => $data['name'],
            'birthday' => $data['birthday'],
            'gender' => $data['gender'],
            'address_id' => $address->id,
        ];

        $personManager = resolve(PersonManager::class);
        $person = $personManager->storePerson($personData);

        if(!$person){
            throw new \Exception('PERSON_DOES_NOT_STORE');
        }

        return $person;
    }
}
