<?php

namespace App\Business;

use App\Models\User;
use PHPOpenSourceSaver\JWTAuth\Facades\JWTAuth;

class AuthManager
{
    public function register(array $data)
    {
        $user = User::create($data);

        return response()->json(['message' => 'User created', 'data' => $user], 201);
    }

    public function login(array $data)
    {
        if(!$token = JWTAuth::attempt($data)) {
            return response()->json(['error' => 'Wrong credentials'], 400);
        }

        return $this->respondWithToken($token);
    }

    public function logout()
    {
        JWTAuth::invalidate(JWTAuth::getToken());
        return response()->json(['message' => 'Logged out'], 200);
    }

    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => config('jwt.ttl'),
        ]);
    }
}
