<?php

use Illuminate\Support\Facades\Route;


Route::prefix('v1')->group(function () {
    Route::post('/register', \App\Http\Controllers\Api\Auth\RegisterController::class);
    Route::post('/login', \App\Http\Controllers\Api\Auth\LoginController::class);
    Route::post('/logout', \App\Http\Controllers\Api\Auth\LogoutController::class);

    Route::get('/persons',\App\Http\Controllers\Person\IndexController::class);

    Route::middleware(['auth:api'])->group(function () {
        Route::post('/persons', \App\Http\Controllers\Person\StoreController::class);
        Route::put('/persons/{id}',\App\Http\Controllers\Person\UpdateController::class);

        Route::post('/addresses', \App\Http\Controllers\Address\StoreController::class);
        Route::get('/addresses',\App\Http\Controllers\Address\IndexController::class);
        Route::put('/addresses/{id}',\App\Http\Controllers\Address\UpdateController::class);

        Route::post('/add-record', \App\Http\Controllers\Record\AddRecordController::class);
    });
});
