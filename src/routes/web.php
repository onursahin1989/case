<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('index');
});

Route::get('/person', function () {
    return view('persons');
})->name('person');

Route::get('/login', function () {
    return view('login');
})->name('login');

Route::get('/addrecord', function () {
    return view('addrecord');
})->name('addrecord');
